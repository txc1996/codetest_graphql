import * as winston from "winston";

const logger = winston.createLogger({
    format: winston.format.combine(
      winston.format.simple(),
      winston.format.colorize(),
    ),
    level: "info",
    transports: [
      new winston.transports.Console(),
    ],
  });

export default logger;
