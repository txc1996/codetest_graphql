import { IContext } from "../utils";

export const User = {
  posts: ({ id }, args, ctx: IContext) => {
    return ctx.prisma.user({ id }).posts();
  },

  containers: ({ id }, args, ctx: IContext) => {
    return ctx.prisma.user({ id }).containers();
  },
};
