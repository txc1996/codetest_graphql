import { IContext } from "../utils";

export const Subscription = {
  feedSubscription: {
    resolve: (payload) => {
      return payload;
    },
    subscribe: async (parent, args, ctx: IContext) => {
      return ctx.prisma.$subscribe
        .post({
          mutation_in: ["CREATED", "UPDATED"],
        })
        .node();
    },
  },

  containerUpdate: {
    resolve: (payload) => {
      return payload;
    },
    subscribe: async (parent, args, ctx: IContext) => {
      return ctx.prisma.$subscribe.container({
        mutation_in: ["CREATED", "UPDATED"],
      }).node();
    },
  },
};
