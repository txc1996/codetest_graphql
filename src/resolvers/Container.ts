import { IContext } from "../utils";

export const Container = {
    createdBy: ({ id }, args, ctx: IContext) => {
        return ctx.prisma.container({ id }).createdBy();
    },
};
