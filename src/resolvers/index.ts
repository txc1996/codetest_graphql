import { Container } from "./Container";
import { auth } from "./Mutation/auth";
import { container } from "./Mutation/container";
import { post } from "./Mutation/post";
import { Post } from "./Post";
import { Query } from "./Query";
import { Subscription } from "./Subscription";
import { User } from "./User";

export default {
  Container,
  Mutation: {
    ...auth,
    ...post,
    ...container,
  },
  Post,
  Query,
  Subscription,
  User,
};
