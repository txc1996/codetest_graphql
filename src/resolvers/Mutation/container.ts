import * as Docker from "dockerode";
import * as jwt from "jsonwebtoken";
import { DockerService } from "../../services/dockerService";
import { IContext, Util } from "../../utils";

export const container = {
    async runContainer(parent, { containerName, imageName, commands, volumeName, volumeDestination }, ctx: IContext) {
        const util = new Util({ ctx, jsonwebtoken: jwt });
        const userId = util.getUserId();
        const docker = new Docker({ socketPath: process.env.DOCKER_SOCKET_PATH });
        const dockerService = new DockerService({ docker, ctx });
        const response: any = await dockerService.createContainer({
            commands,
            containerName,
            imageName,
            volumeDestination,
            volumeName,
        });
        if (response.id) {
            const dataContainer = await ctx.prisma.createContainer({
                containerId: response.id,
                createdBy: {
                    connect: { id: userId },
                },
                image: imageName,
                name: containerName,
                state: "CREATED",
            });

            if (!dataContainer) {
                throw new Error(`Fail to write container to database: ${ dataContainer }`);
            }

            dockerService.runContainerAndUpdateState({ container: response });

            return dataContainer;
        } else {
            throw new Error(`${ response }`);
        }
    },

    async destroyContainer(parent, { idOrName, removeVolume, force, link }, ctx: IContext) {
        const util = new Util({ ctx, jsonwebtoken: jwt });
        const userId = util.getUserId();
        const docker = new Docker({ socketPath: process.env.DOCKER_SOCKET_PATH });
        const dockerService = new DockerService({ docker, ctx });
        const removeError = await dockerService.removeContainer({
            force,
            idOrName,
            link,
            removeVolume,
        });

        if (removeError) {
            throw new Error(`${ removeError }`);
        }

        const containers: any = await ctx.prisma.containers({
            first: 1,
            where: {
                OR: [
                    { containerId: idOrName },
                    { name: idOrName },
                ],
                createdBy: { id: userId },
                state_not: "DESTROYED",
            },
        });

        if (containers.length === 0) {
            throw new Error(`Container not found`);
        }

        return ctx.prisma.updateContainer({
            data: { state: "DESTROYED" },
            where: { id: containers[0].id },
        });
    },
};
