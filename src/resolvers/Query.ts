import * as Docker from "dockerode";
import * as jwt from "jsonwebtoken";
import { DockerService } from "../services/dockerService";
import { IContext, Util } from "../utils";

export const Query = {
  feed(parent, args, ctx: IContext) {
    return ctx.prisma.posts({ where: { published: true } });
  },

  drafts(parent, args, ctx: IContext) {
    const util = new Util({ ctx, jsonwebtoken: jwt });
    const id = util.getUserId();

    const where = {
      author: {
        id,
      },
      published: false,
    };

    return ctx.prisma.posts({ where });
  },

  post(parent, { id }, ctx: IContext) {
    return ctx.prisma.post({ id });
  },

  me(parent, args, ctx: IContext) {
    const util = new Util({ ctx, jsonwebtoken: jwt });
    const id = util.getUserId();
    return ctx.prisma.user({ id });
  },

  activeContainers(parent, args, ctx: IContext) {
    const util = new Util({ ctx, jsonwebtoken: jwt });
    const id = util.getUserId();
    const docker = new Docker({ socketPath: process.env.DOCKER_SOCKET_PATH });
    const dockerService = new DockerService({ docker, ctx });
    return dockerService.listContainers();
  },

  containersCreatedByApiUser(parent, args, ctx: IContext) {
    const util = new Util({ ctx, jsonwebtoken: jwt });
    const id = util.getUserId();
    return ctx.prisma.containers();
  },
};
