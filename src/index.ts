import { GraphQLServer } from "graphql-yoga";
import { prisma } from "./generated/prisma-client";
import logger from "./log/winston";
import resolvers from "./resolvers";

const server = new GraphQLServer({
  context: (request) => ({
    ...request,
    prisma,
  }),
  resolvers,
  typeDefs: "./src/schema.graphql",
});

// tslint:disable-next-line:no-console
server.start(() => logger.info(`Server is running on http://localhost:4000`));
