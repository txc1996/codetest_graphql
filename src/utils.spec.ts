import * as sinon from "sinon";
import { IContext, Util } from "./utils";

describe("getUserId", () => {

  afterEach(() => {
    sinon.restore();
  });

  it("should load data from Authorization header", () => {
    const testContext = {} as IContext;
    testContext.request = {};
    testContext.request.get = jest.fn((x) => x);

    const fakeJwt = {} as any;
    const util = new Util({ ctx: testContext, jsonwebtoken: fakeJwt });

    expect(() => {
      util.getUserId();
    }).toThrowError();

    expect(testContext.request.get).toBeCalled();
    expect(testContext.request.get).toBeCalledWith("Authorization");
  });

  it("should ignore jwt verification without a token", () => {
    const testContext = {} as IContext;
    testContext.request = {
      get: jest.fn((x) => ""),
    };

    const verify = jest.fn();
    const fakeJwt = {
      verify,
    } as any;

    const util = new Util({ctx: testContext, jsonwebtoken: fakeJwt});

    expect(() => {
      util.getUserId();
    }).toThrow("Not authorized");

    expect(verify).toHaveBeenCalledTimes(0);
  });

  it("should verify with jwt if a token is present", () => {
    const testContext = {} as IContext;
    testContext.request = {
      get: jest.fn((x) => (x === "Authorization" ? "Bearer tokenX" : x)),
    };

    const verify = jest.fn(() => ({ userId: "U2018" }));
    const fakeJwt = {
      verify,
    };

    const util = new Util({ctx: testContext, jsonwebtoken: fakeJwt});

    expect(util.getUserId()).toBe("U2018");

    // jwt.verify was called once with tokenX as the token param
    expect(verify).toBeCalledTimes(1);
    expect(verify.mock.calls[0][0]).toBe("tokenX");
  });
});
