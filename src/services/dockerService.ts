import logger from "../log/winston";
import { IContext } from "../utils";

export class DockerService {
    private docker: any;
    private ctx: IContext;

    constructor({ docker, ctx }) {
        this.docker = docker;
        this.ctx = ctx;
    }

    public listContainers() {
        const listContainer = [];
        return new Promise((resolve, reject) => {
            this.docker.listContainers({all: true}, (err, containers) => {
                if (err) {
                    reject(err);
                } else {
                    containers.forEach((container) => {
                        listContainer.push({
                            containerId: container.Id,
                            image: container.Image,
                            name: container.Names[0],
                            state: container.State,
                        });
                    });

                    resolve(listContainer);
                }
            });
        });
    }

    public createContainer({ containerName, imageName, commands, volumeName, volumeDestination }) {
        return new Promise((resolve, reject) => {
            this.docker.createContainer({
                Cmd: commands,
                HostConfig: {
                    Mounts: [{
                        Source: volumeName,
                        Target: "/data/" + volumeDestination,
                        Type: "volume",
                    }],
                },
                Image: imageName,
                name: containerName,
            }, (err, container) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(container);
                }
            });
        });
    }

    public runContainerAndUpdateState({ container }) {
        container.start( async (err, data) => {
            if (!err) {
                const dContainer: any = await this.ctx.prisma.containers({
                    first: 1,
                    where: {
                      AND: [
                        { containerId: container.id },
                        { state_not: "DESTROYED" },
                      ],
                    },
                });

                if (dContainer.lenght !== 0) {
                    await this.ctx.prisma.updateContainer({
                        data: { state: "RUNNING" },
                        where: { id: dContainer[0].id },
                    });
                } else {
                    logger.error(`${dContainer}`);
                }

                container.wait( async (waitErr, waitData) => {
                    if (!waitErr) {
                        await this.ctx.prisma.updateContainer({
                            data: { state: "EXITED" },
                            where: { id: dContainer[0].id },
                        });
                    } else {
                        logger.error(`Fail to wait container ${container.id}: ${err}`);
                    }
                });
            } else {
                logger.error(`Fail to start container ${container.id}: ${err}`);
            }
        });
    }

    public removeContainer({ idOrName, removeVolume, force, link }) {
        return new Promise((resovle, reject) => {
            const container = this.docker.getContainer(idOrName);
            container.remove({ v: removeVolume, force, link }, (err, data) => {
                resovle(err);
            });
        });
    }
}
