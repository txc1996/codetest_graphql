import { Prisma } from "./generated/prisma-client";

export interface IContext {
  prisma: Prisma;
  request: any;
}
export class Util {
  private ctx: IContext;
  private jwt: any;

  constructor({ctx, jsonwebtoken}) {
    this.ctx = ctx;
    this.jwt = jsonwebtoken;
  }

  public getUserId() {
    const Authorization = this.ctx.request.get("Authorization");
    if (Authorization) {
      const token = Authorization.replace("Bearer ", "");
      const { userId } = this.jwt.verify(token, process.env.APP_SECRET) as { userId: string};
      return userId;
    }

    throw new Error(`Not authorized`);
  }
}
