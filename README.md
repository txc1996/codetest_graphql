# TypeScript & GraphQL code test

by [GPU Exchange](https://gpu.exchange).

This is a coding exercise based on the TypeScript GraphQL advanced [boilerplate](https://github.com/graphql-boilerplates/typescript-graphql-server/tree/master/advanced).

Although this test is un-timed, the basic component of this test should take no more than 2h to finish. We expect you to finish it in 1h.

Please redirect any questions you have regarding this test to your interviewer.

## Submission guide

To complete this code test:

* Fork this repository
* Work on the code component
* Write your awsers in the file `ANSWERS.md` and place it at the project root.
* Send a public link to your repository to the interviewer at GX. 

## Tasks

### Setup
* Download and run the code.
* Run the playground and create a user account, and login in a separate request to verify that GraphQL works.

### Unit testing
* Run the unit tests, note that this repository contains a `bitbucket-pipelines.yml` file.
* Go through the test files and finish the TODOs.
* Pay special attention to utils.spec.ts. (Hint: dependency injection)

### Project structure

This project contains a security flaw due to the way it was set up (check the very first commits). Find out what it is and explain it in your report.

### Docker Integration (New feature development)

This is an advanced section of the test and will take more time than the 1h mentioned above. This section is optional and you don't need to complete it, unless instructed by the interviewer.

Final interview is guaranteed for applicants who successfully complete this section, in addition to the previous tasks.

For this task, we are going to build a Docker integration for the API. Write a new set of GraphQL queries and mutations that does the following.

* This feature is only applicable to authenticated users.
* List all active containers on the API host.
* Launch a container with a volume mount to /data/v147 where `v147` is a mutation parameter.
* List all containers launched by a particular API user.
* Allow the user to destroy containers they created.
* The system must keep track of a user's launched containers (including those destroyed).
* The system should return immediately following the launch/destroy command, while allowing users to keep track of the container status later (i.e. with GraphQL subscription). 

For this test, assume a Docker server is located on the same host as the Node server, and that the Node user can access the docker daemon via local pipe. You need to set up your machine accordingly to finish this section.